package com.arcbees.shell.cli;

/**
 * A no-op {@link CliConfigurer}.
 */
abstract class NullCliConfigurer implements CliConfigurer {
}
