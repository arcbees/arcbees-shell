package com.arcbees.shell.cli;

import java.util.List;

import net.sourceforge.argparse4j.inf.Namespace;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Inject this class in your {@link com.arcbees.shell.environment.Environment Environment}s to read command-line
 * arguments.
 */
public class Cli {
    private final Namespace namespace;

    Cli(Namespace namespace) {
        this.namespace = namespace;
    }

    /**
     * Get the first command-line argument matching the given `key` configured in a {@link CliConfigurer}.
     *
     * @param key the key configured in the `CliConfigurer` for this argument
     * @param <T> the type configured in the `CliConfigurer` for this argument
     * @return the argument value, its default value configured in `CliConfigurer` or `null`
     * @throws ClassCastException if `T` is not a class compatible with the one configured for this `key`
     */
    public <T> T get(String key) {
        return namespace.get(formatKey(key));
    }

    /**
     * Get all command-line arguments matching the given `key` configured in a {@link CliConfigurer}.
     *
     * Gotcha: You must tell ArgParse4j that this argument supports multiple values by calling
     * {@link net.sourceforge.argparse4j.inf.Argument#nargs(String) Argument#nargs(String)} in your configurer.
     *
     * @param key the key configured in the `CliConfigurer` for this argument
     * @param <T> the type configured in the `CliConfigurer` for this argument
     * @return the argument value, its default value configured in `CliConfigurer` or an empty list
     * @throws ClassCastException if `T` is not a class compatible with the one configured for this `key`
     */
    public <T> List<T> getAll(String key) {
        try {
            List<T> list = namespace.getList(formatKey(key));
            return list == null ? emptyList() : list;
        } catch (ClassCastException ignore) {
            return singletonList(get(key));
        }
    }

    private String formatKey(String key) {
        // Because the parser replaces - by _, but doesn't do it when we retrieve values...
        return key.replace('-', '_');
    }
}
