/**
 * The CLI library allows {@link com.arcbees.shell.environment.Environment Environment}s to use command line
 * parameters to customize their properties.
 *
 * To use it, all you need to do is have it available in your classpath (add it to your pom.xml) and
 * annotate all {@link com.arcbees.shell.environment.Environment Environment}s configured via
 * {@link com.arcbees.shell.ArcbeesShellConfiguration#addEnvironment(java.lang.Class) addEnvironment()} with
 * {@link com.arcbees.shell.cli.CliEnvironment @CliEnvironment}.
 *
 * The CLI will read command line arguments individually, from text files or a combination of both. For  example,
 * the java program could be called with the following command line:
 *
 * ```bash
 * java -server -jar application.jar @global.args --timezone "Europe/Paris"
 * ```
 *
 * and **global.args**:
 *
 * ```text
 * --locale
 * en-CA
 * --timezone
 * America/Toronto
 * ```
 *
 * The arguments are read sequentially so application.jar would receive the `en-CA` locale and the `Europe/Paris`
 * timezone.
 *
 * This library automatically configures the {@link com.arcbees.shell.environment.GlobalEnvironment GlobalEnvironment}
 * to support command-line parameters.
 *
 * [ArgParse4j](https://argparse4j.github.io/) is used to manage the command-line.
 *
 * @see com.arcbees.shell.cli.CliEnvironment @CliEnvironment
 * @see com.arcbees.shell.cli.CliGlobalEnvironment CliGlobalEnvironment
 */
package com.arcbees.shell.cli;
