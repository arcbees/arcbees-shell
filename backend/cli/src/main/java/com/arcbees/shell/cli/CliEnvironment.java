package com.arcbees.shell.cli;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Tells the {@link CliFactory} which class configures the acceptable command-line parameters for the annotated
 * {@link com.arcbees.shell.environment.Environment Environment}.
 *
 * By default, if an `Environment` is configured in the Arcbees Shell and this annotation is not present, a runtime
 * warning will be generated during the bootstrap sequence. To disable this warning, annotate the environment with
 * `@{@link CliEnvironment}` without specifying a `{@link CliConfigurer}`. The arguments can be read via an instance
 * of {@link Cli}, typically injected in the environment.
 *
 * Exemple:
 *
 * ```java
 * class MyApplication {
 *     ArcbeesShell.configure()
 *             .addEnvironment(SqlEnvironment.class)
 *             .start();
 * }
 *
 * .@CliEnvironment(configurer = SqlCliConfigurer.class)
 * class SqlEnvironment implements Environment {
 *     private static final String HOST_ARGUMENT = "pg-host";
 *
 *     private final Cli cli;
 *
 *     .@Inject
 *     SqlEnvironment(Cli cli) {
 *         this.cli = cli;
 *     }
 *
 *     public String getHost() {
 *         return cli.get(HOST_ARGUMENT);
 *     }
 *
 *     static class SqlCliConfigurer implements CliConfigurer {
 *         .@Override
 *         public void configureParser(ArgumentParser parser) {
 *             ArgumentGroup group = parser.addArgumentGroup("PostgreSQL configurations");
 *             group.addArgument("--" + PG_HOST_ARGUMENT)
 *                     .setDefault("localhost")
 *                     .help("the host on which the PostgreSQL server is listening")
 *                     .metavar("URL");
 *         }
 *     }
 * }
 * ```
 *
 * @see Cli
 * @see CliConfigurer
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CliEnvironment {
    /**
     * Setup the appropriate {@link CliConfigurer} to use for the annotated
     * {@link com.arcbees.shell.environment.Environment Environment}. The `CliConfigurer` must have a zero-arg
     * constructor or else an error log will be printed during the bootstrap sequence. Such an error will not prevent
     * the application from starting, but the desired command-line parameters will *not* be read.
     *
     * @return a {@link CliConfigurer} implementation or a no-op configurer if left blank
     */
    Class<? extends CliConfigurer> configurer() default NullCliConfigurer.class;
}
