package com.arcbees.shell.cli;

import javax.inject.Inject;

import com.arcbees.shell.cli.CliGlobalEnvironment.GlobalEnvironmentConfigurer;
import com.arcbees.shell.environment.GlobalEnvironment;

import net.sourceforge.argparse4j.inf.ArgumentGroup;
import net.sourceforge.argparse4j.inf.ArgumentParser;

/**
 * A default implementation of {@link GlobalEnvironment} that is automatically configured if the CLI library is present
 * in your classpath.
 *
 * This implementation will add two accepted parameters to the command-line configuration:
 *
 * * `--locale`, default is fr-CA
 * * `--timezone`, default is UTC
 */
@CliEnvironment(configurer = GlobalEnvironmentConfigurer.class)
class CliGlobalEnvironment implements GlobalEnvironment {
    private static final String LOCALE_ARGUMENT = "locale";
    private static final String TIMEZONE_ARGUMENT = "timezone";

    private final Cli cli;

    @Inject
    CliGlobalEnvironment(Cli cli) {
        this.cli = cli;
    }

    @Override
    public String getLocale() {
        return cli.get(LOCALE_ARGUMENT);
    }

    @Override
    public String getTimezone() {
        return cli.get(TIMEZONE_ARGUMENT);
    }

    static class GlobalEnvironmentConfigurer implements CliConfigurer {
        @Override
        public void configureParser(ArgumentParser parser) {
            ArgumentGroup group = parser.addArgumentGroup("Global configurations");
            group.addArgument("--" + LOCALE_ARGUMENT)
                    .help("The default locale to be used by the application.")
                    .setDefault("fr-CA")
                    .metavar("LOCALE");

            group = parser.addArgumentGroup("Developer configurations");
            group.addArgument("--" + TIMEZONE_ARGUMENT)
                    .help("The default timezone to be used by the application.")
                    .setDefault("UTC")
                    .metavar("ZONE");
        }
    }
}
