package com.arcbees.shell.cli;

import javax.inject.Singleton;

import com.arcbees.shell.environment.GlobalEnvironment;
import com.google.inject.AbstractModule;

class CliModule extends AbstractModule {
    private final Cli cli;

    CliModule(Cli cli) {
        this.cli = cli;
    }

    @Override
    protected void configure() {
        bind(Cli.class).toInstance(cli);
        bind(GlobalEnvironment.class).to(CliGlobalEnvironment.class).in(Singleton.class);
    }
}
