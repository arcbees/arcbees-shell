package com.arcbees.shell.cli;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arcbees.shell.ShellExecutionResult;
import com.arcbees.shell.ShellInitializationException;
import com.arcbees.shell.environment.Environment;
import com.arcbees.shell.environment.EnvironmentFactory;
import com.google.inject.Module;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.helper.HelpScreenException;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import static java.util.function.Function.identity;

/**
 * The CLI implementation of {@link EnvironmentFactory}, registered in Java's
 * {@link java.util.ServiceLoader ServiceLoader}.
 *
 * This class is automatically instantiated and called by the Arcbees Shell in the bootstrap sequence. You have no
 * need to use it directly.
 */
public class CliFactory implements EnvironmentFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CliFactory.class);

    @Override
    public Module create(
            String name,
            Collection<Class<? extends Environment>> environmentClasses,
            String[] args) throws ShellInitializationException {
        ArgumentParser parser = createParser(name);
        configureParser(environmentClasses, parser);

        try {
            Namespace namespace = parser.parseArgs(args);
            Cli cli = new Cli(namespace);
            return new CliModule(cli);
        } catch (HelpScreenException e) {
            parser.handleError(e);
            throw new ShellInitializationException("CLI help requested.", ShellExecutionResult.SUCCESS);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            throw new ShellInitializationException("Could not parse CLI arguments.", e);
        }
    }

    private void configureParser(Collection<Class<? extends Environment>> environmentClasses, ArgumentParser parser) {
        Stream.of(Stream.of(CliGlobalEnvironment.class), environmentClasses.stream())
                .flatMap(identity())
                .map(this::loadCliConfigurer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(c -> c.configureParser(parser));
    }

    private ArgumentParser createParser(String name) {
        return ArgumentParsers.newFor(name)
                .addHelp(true)
                .singleMetavar(true)
                .prefixChars("-")
                .fromFilePrefix("@")
                .locale(Locale.ENGLISH)
                .build()
                .usage("java -jar ${prog}.jar ARGUMENTS")
                .description("Start the application.")
                .defaultHelp(true);
    }

    private Optional<CliConfigurer> loadCliConfigurer(Class<? extends Environment> environmentClass) {
        CliEnvironment config = environmentClass.getAnnotation(CliEnvironment.class);

        if (config == null) {
            LOGGER.warn("Environment '{}' is not annotated with @{}. Did you forget to configure it?",
                    environmentClass.getCanonicalName(), CliEnvironment.class.getSimpleName());
            return Optional.empty();
        }

        Class<? extends CliConfigurer> configurerClass = config.configurer();
        if (configurerClass == NullCliConfigurer.class) {
            return Optional.empty();
        }

        try {
            Constructor<? extends CliConfigurer> constructor = configurerClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            return Optional.of(constructor.newInstance());
        } catch (IllegalAccessException
                | InstantiationException
                | InvocationTargetException
                | NoSuchMethodException e) {
            LOGGER.error("Could not instantiate configurer '{}'. Make sure the class is accessible and instantiable "
                    + "with a public no-arg constructor.", configurerClass.getCanonicalName(), e);
            return Optional.empty();
        }
    }
}
