package com.arcbees.shell.cli;

import net.sourceforge.argparse4j.inf.ArgumentParser;

/**
 * Configure the parameters allowed in the program's command-line.
 *
 * @see Cli
 * @see CliEnvironment
 */
public interface CliConfigurer {
    /**
     * This method is calld during the bootstrap process to tell the CLI how to read command-line arguments.
     *
     * Implementations will be forcibly coupled to ArgParse4j. It is recommended that you read their documentation
     * for more details on how to configure the command-line
     *
     * @param parser the ArgParse4j parser
     * @see "[ArgParse4j for more help on configuring the parser](https://argparse4j.github.io/usage.html)"
     */
    void configureParser(ArgumentParser parser);
}
