package com.arcbees.shell;

import java.util.Collections;
import java.util.Set;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import com.arcbees.shell.application.Application;
import com.arcbees.shell.application.ApplicationService;
import com.google.inject.Guice;

import static java.util.Arrays.asList;

import static org.assertj.core.api.Assertions.assertThat;

class ShellModuleTest {
    @Inject
    Application application;
    @Inject
    Set<ApplicationService> applicationServices;

    @Test
    @SuppressWarnings("unchecked")
    void configure_bindsAllApplicationServices() {
        Class<? extends ApplicationService>[] services = new Class[] {
                FirstApplicationService.class,
                SecondApplicationService.class
        };
        ShellModule shellModule = new ShellModule(asList(services));

        Guice.createInjector(shellModule).injectMembers(this);

        assertThat(this.applicationServices).hasSize(2);
        assertThat(this.applicationServices.stream().filter(a -> a instanceof FirstApplicationService))
                .hasSize(1);
        assertThat(this.applicationServices.stream().filter(a -> a instanceof SecondApplicationService))
                .hasSize(1);
    }

    @Test
    void configure_bindsApplication() {
        ShellModule shellModule = new ShellModule(Collections.singletonList(FirstApplicationService.class));

        Guice.createInjector(shellModule).injectMembers(this);

        assertThat(application).isNotNull();
    }

    static class FirstApplicationService implements ApplicationService { }

    static class SecondApplicationService implements ApplicationService { }
}
