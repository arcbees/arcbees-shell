package com.arcbees.shell.it.utils;

import java.util.Collection;

import com.arcbees.shell.environment.Environment;
import com.arcbees.shell.environment.EnvironmentFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Module;

public class NoOpEnvironmentFactory implements EnvironmentFactory {
    @Override
    public Module create(String name, Collection<Class<? extends Environment>> environmentClasses, String[] args) {
        return new AbstractModule() {
            @Override
            protected void configure() {
            }
        };
    }
}
