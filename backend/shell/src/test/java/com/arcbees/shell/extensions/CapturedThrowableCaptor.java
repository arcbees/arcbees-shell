package com.arcbees.shell.extensions;

import java.util.Optional;

public interface CapturedThrowableCaptor {
    Optional<CapturedThrowable> find(Class<? extends Throwable> classy);
}
