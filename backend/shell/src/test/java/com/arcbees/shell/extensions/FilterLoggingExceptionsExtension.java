package com.arcbees.shell.extensions;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.spi.FilterReply;

import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;

public class FilterLoggingExceptionsExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback,
        ParameterResolver {
    private final Logger rootLogger;

    public FilterLoggingExceptionsExtension() {
        this.rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
    }

    @Override
    public void beforeTestExecution(ExtensionContext context) {
        Store store = getStore(context);
        Set<Class<? extends Throwable>> throwables = findExpectedThrowables(context);
        TurboFilter filter = new StripTrowablesFilter(store, throwables);

        store.put(TurboFilter.class, filter);
        rootLogger.getLoggerContext().addTurboFilter(filter);
    }

    private Set<Class<? extends Throwable>> findExpectedThrowables(ExtensionContext context) {
        return context.getElement()
                .map(a -> a.getAnnotationsByType(ExpectLoggedException.class))
                .map(Stream::of)
                .orElse(Stream.empty())
                .map(ExpectLoggedException::value)
                .collect(toSet());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) {
        Store store = getStore(context);
        TurboFilter filter = (TurboFilter) store.remove(TurboFilter.class);

        rootLogger.getLoggerContext().getTurboFilterList().remove(filter);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == CapturedThrowableCaptor.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return new StoreBackedCapturedThrowableCaptor(getStore(extensionContext));
    }

    private Store getStore(ExtensionContext context) {
        return context.getStore(Namespace.create(getClass(), context));
    }

    static class StripTrowablesFilter extends TurboFilter {
        private final Set<Class<? extends Throwable>> expectedThrowables;
        private final Set<CapturedThrowable> capturedThrowables;

        StripTrowablesFilter(Store store, Set<Class<? extends Throwable>> expectedThrowables) {
            this.expectedThrowables = expectedThrowables;
            this.capturedThrowables = new LinkedHashSet<>();

            store.put(CapturedThrowable.class, unmodifiableSet(capturedThrowables));
        }

        @SuppressWarnings("unchecked")
        @Override
        public FilterReply decide(Marker marker, Logger logger, Level level, String format, Object[] params,
                Throwable throwable) {
            boolean isExpected = expectedThrowables.stream().anyMatch(c -> c.isInstance(throwable));

            if (isExpected) {
                capturedThrowables.add(new LogbackCapturedThrowable(throwable, format, params));
                return FilterReply.DENY;
            }
            return FilterReply.NEUTRAL;
        }
    }
}
