package com.arcbees.shell.extensions;

import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.extension.ExtensionContext.Store;

class StoreBackedCapturedThrowableCaptor implements CapturedThrowableCaptor {
    private final Store store;

    StoreBackedCapturedThrowableCaptor(Store store) {
        this.store = store;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Optional<CapturedThrowable> find(Class<? extends Throwable> classy) {
        Set<CapturedThrowable> throwables = (Set<CapturedThrowable>) store.get(CapturedThrowable.class);
        return throwables.stream()
                .filter(t -> t.getThrowable().getClass() == classy)
                .findAny();
    }
}
