package com.arcbees.shell;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.arcbees.shell.application.ApplicationService;
import com.arcbees.shell.environment.GlobalEnvironment;
import com.google.inject.Module;

import static org.assertj.core.api.Assertions.assertThat;

class ArcbeesShellConfigurationTest {
    ArcbeesShellConfiguration shellConfiguration;

    @BeforeEach
    void setUp() {
        shellConfiguration = new ArcbeesShellConfiguration();
    }

    @Test
    void name_default_isNotEmpty() {
        assertThat(shellConfiguration.name).isNotEmpty();
    }

    @Test
    void name() {
        String name = "Arcbees";

        ArcbeesShellConfiguration configRef = shellConfiguration.name(name);

        assertThat(shellConfiguration.name).isEqualTo(name);
        assertThat(configRef).isSameAs(shellConfiguration);
    }

    @Test
    void arguments_default_isEmpty() {
        assertThat(shellConfiguration.arguments).isEmpty();
    }

    @Test
    void arguments() {
        String[] arguments = {"a", "b", "c"};

        ArcbeesShellConfiguration configRef = shellConfiguration.arguments(arguments);

        assertThat(shellConfiguration.arguments).isEqualTo(arguments);
        assertThat(configRef).isSameAs(shellConfiguration);
    }

    @Test
    void applicationServiceClasses_default_isEmpty() {
        assertThat(shellConfiguration.arguments).isEmpty();
    }

    @Test
    void applicationService() {
        Class<ApplicationService> classy = ApplicationService.class;

        ArcbeesShellConfiguration configRef = shellConfiguration.addApplicationService(classy);

        assertThat(shellConfiguration.applicationServiceClasses).containsExactly(classy);
        assertThat(configRef).isSameAs(shellConfiguration);
    }

    @Test
    void environmentClasses_default_isEmpty() {
        assertThat(shellConfiguration.environmentClasses).isEmpty();
    }

    @Test
    void environment() {
        Class<GlobalEnvironment> classy = GlobalEnvironment.class;

        ArcbeesShellConfiguration configRef = shellConfiguration.addEnvironment(classy);

        assertThat(shellConfiguration.environmentClasses).containsExactly(classy);
        assertThat(configRef).isSameAs(shellConfiguration);
    }

    @Test
    void moduleClasses_default_isEmpty() {
        assertThat(shellConfiguration.arguments).isEmpty();
    }

    @Test
    void module() {
        Class<? extends Module> classy = ShellModule.class;

        ArcbeesShellConfiguration configRef = shellConfiguration.addModule(classy);

        assertThat(shellConfiguration.moduleClasses).containsExactly(classy);
        assertThat(configRef).isSameAs(shellConfiguration);
    }
}
