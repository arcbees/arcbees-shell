package com.arcbees.shell.it;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.arcbees.shell.ArcbeesShell;
import com.arcbees.shell.ShellExecutionResult;
import com.arcbees.shell.ShellInitializationException;
import com.arcbees.shell.environment.EnvironmentFactory;
import com.arcbees.shell.extensions.CapturedThrowable;
import com.arcbees.shell.extensions.CapturedThrowableCaptor;
import com.arcbees.shell.extensions.ExpectLoggedException;
import com.arcbees.shell.extensions.FilterLoggingExceptionsExtension;
import com.arcbees.shell.it.utils.NoOpEnvironmentFactory;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import static org.assertj.core.api.Assertions.assertThat;

import static com.arcbees.shell.it.utils.ServiceProviderTestUtil.runWithServiceProviders;

@ExtendWith(FilterLoggingExceptionsExtension.class)
class ArcbeesShellBootstrapIT {
    @Test
    @ExpectLoggedException(value = ShellInitializationException.class)
    void startShell_classpathWithoutAnEnvironmentFactory_throws(CapturedThrowableCaptor throwableCaptor) {
        ArcbeesShell shell = ArcbeesShell.configure().createShell();

        ShellExecutionResult result = shell.start(false);

        assertThat(result).isEqualTo(ShellExecutionResult.ERROR);

        Optional<CapturedThrowable> initException = throwableCaptor.find(ShellInitializationException.class);
        assertThat(initException).isPresent();
        assertThat(initException.get().getThrowable().getMessage())
                .contains("No implementations of EnvironmentFactory exist.");
    }

    @Test
    @ExpectLoggedException(value = ShellInitializationException.class)
    void startShell_noGlobalEnvironmentImplementation_throws(CapturedThrowableCaptor throwableCaptor) {
        Multimap<Class<?>, Class<?>> providers = HashMultimap.create();
        providers.put(EnvironmentFactory.class, NoOpEnvironmentFactory.class);

        runWithServiceProviders(providers.asMap(), () -> {
            ArcbeesShell shell = ArcbeesShell.configure().createShell();
            ShellExecutionResult result = shell.start(false);

            assertThat(result).isEqualTo(ShellExecutionResult.ERROR);

            Optional<CapturedThrowable> initException = throwableCaptor.find(ShellInitializationException.class);
            assertThat(initException).isPresent();
            assertThat(initException.get().getThrowable().getMessage())
                    .contains("Could not find an implementation of GlobalEnvironment.");
        });
    }
}
