package com.arcbees.shell.it.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Throwables;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import static java.util.stream.Collectors.toSet;

public class ServiceProviderTestUtil {
    public static void runWithServiceProviders(Map<Class<?>, Collection<Class<?>>> services, Runnable runnable) {
        Thread thread = new Thread(runnable);

        URLClassLoader classLoader = createClassLoaderWithServiceProviders(services);
        thread.setContextClassLoader(classLoader);

        Throwable[] uncaughtException = new Throwable[1];
        thread.setUncaughtExceptionHandler((t, throwable) -> uncaughtException[0] = throwable);

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException ignore) {
        }

        if (uncaughtException[0] != null) {
            Throwables.throwIfUnchecked(uncaughtException[0]);
            throw new RuntimeException(uncaughtException[0]);
        }
    }

    private static URLClassLoader createClassLoaderWithServiceProviders(Map<Class<?>, Collection<Class<?>>> services) {
        Path filesystem = createFileSystemWithServiceProviders(services);
        ClassLoader parentClassLoader = Thread.currentThread().getContextClassLoader();

        try {
            URL classpathRoot = filesystem.toUri().toURL();
            return new URLClassLoader(new URL[] {classpathRoot}, parentClassLoader);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private static Path createFileSystemWithServiceProviders(Map<Class<?>, Collection<Class<?>>> services) {
        try {
            FileSystem fileSystem = Jimfs.newFileSystem(Configuration.unix());
            Path javaPath = fileSystem.getPath("/java");
            javaPath = Files.createDirectory(javaPath);

            Path servicePath = javaPath.resolve("META-INF/services");
            servicePath = Files.createDirectories(servicePath);

            createServiceProviders(servicePath, services);

            return javaPath;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createServiceProviders(Path path, Map<Class<?>, Collection<Class<?>>> services)
            throws IOException {
        for (Entry<Class<?>, Collection<Class<?>>> entry : services.entrySet()) {
            String abstraction = entry.getKey().getCanonicalName();
            Collection<String> implementations = entry.getValue().stream()
                    .map(Class::getCanonicalName)
                    .collect(toSet());

            Path file = path.resolve(abstraction);
            Files.write(file, implementations, StandardCharsets.UTF_8);
        }
    }
}
