package com.arcbees.shell.extensions;

import java.text.MessageFormat;

class LogbackCapturedThrowable implements CapturedThrowable {
    private final Throwable throwable;
    private final String format;
    private final Object[] params;

    LogbackCapturedThrowable(Throwable throwable, String format, Object[] params) {
        this.throwable = throwable;
        this.format = format;
        this.params = params;
    }

    @Override
    public Throwable getThrowable() {
        return throwable;
    }

    @Override
    public String getMessage() {
        return MessageFormat.format(format, params);
    }
}
