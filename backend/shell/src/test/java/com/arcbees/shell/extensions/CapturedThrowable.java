package com.arcbees.shell.extensions;

public interface CapturedThrowable {
    Throwable getThrowable();

    String getMessage();
}
