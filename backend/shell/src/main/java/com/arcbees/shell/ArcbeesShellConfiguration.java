package com.arcbees.shell;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.arcbees.shell.application.ApplicationService;
import com.arcbees.shell.environment.Environment;
import com.google.inject.Module;

/**
 * Fluent configuration methods for the {@link ArcbeesShell}. To use it, call {@link ArcbeesShell#configure()}.
 */
public class ArcbeesShellConfiguration {
    String name = "Application";
    String[] arguments = new String[0];
    Set<Class<? extends Environment>> environmentClasses = new LinkedHashSet<>();
    Set<Class<? extends Module>> moduleClasses = new LinkedHashSet<>();
    Set<Class<? extends ApplicationService>> applicationServiceClasses = new LinkedHashSet<>();

    ArcbeesShellConfiguration() {
    }

    /**
     * The static name used to describe this application.
     *
     * Defaults to "**Application**".
     *
     * @param name the application name
     * @return this instance
     */
    public ArcbeesShellConfiguration name(String name) {
        this.name = name;
        return this;
    }

    /**
     * The program arguments used to configure {@link Environment}s.
     *
     * This will often be the arguments passed to a main function:
     * ```java
     * public class Main {
     *   public static void main(String[] args) {
     *     ArcbeesShell.configure()
     *         .name("Arcbees Is Awesome")
     *         .arguments(args)
     *         .start();
     *   }
     * }
     * ```
     *
     * @param arguments the program arguments
     * @return this instance
     */
    public ArcbeesShellConfiguration arguments(String[] arguments) {
        this.arguments = arguments == null ? new String[0] : arguments.clone();
        return this;
    }

    /**
     * Add an {@link ApplicationService} class that will be managed by the root
     * {@link com.arcbees.shell.application.Application Application}. All classes provided here will see their
     * dependencies injected.
     *
     * @param service an injectable {@link ApplicationService} class
     * @return this instance
     * @see com.arcbees.shell.injector.InjectorFactory InjectorFactory
     */
    public ArcbeesShellConfiguration addApplicationService(Class<? extends ApplicationService> service) {
        applicationServiceClasses.add(service);
        return this;
    }

    /**
     * Add an {@link Environment} class that will be available throughout the application. Environment classes can be
     * injected with explicit bindings configured in the {@link Module} returned by
     * {@link com.arcbees.shell.environment.EnvironmentFactory#create(String, Collection, String[])
     * EnvironmentFactory#create(...) }.
     *
     * @param environmentClass an injectable {@link Environment} class
     * @return this instance
     * @see com.arcbees.shell.environment.EnvironmentFactory EnvironmentFactory
     */
    public ArcbeesShellConfiguration addEnvironment(Class<? extends Environment> environmentClass) {
        environmentClasses.add(environmentClass);
        return this;
    }

    /**
     * Add a {@link Module} class that will be passed through the
     * {@link com.arcbees.shell.injector.InjectorFactory InjectorFactory}. Module classes will be injected with
     * {@link Environment}s configured with {@link #addEnvironment(Class)}.
     *
     * @param moduleClass an injectable {@link Module} class
     * @return this instance
     */
    public ArcbeesShellConfiguration addModule(Class<? extends Module> moduleClass) {
        moduleClasses.add(moduleClass);
        return this;
    }

    /**
     * Create an {@link ArcbeesShell} instance, allowing for extra operations before starting it.
     *
     * @return a shell instance
     */
    public ArcbeesShell createShell() {
        return new ArcbeesShell(this);
    }

    /**
     * Create and immediately start an {@link ArcbeesShell}.
     */
    public void start() {
        createShell().start();
    }
}
