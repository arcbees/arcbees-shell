/**
 * Dependency injection management. Although the {@link com.arcbees.shell.injector.InjectorFactory InjectorFactory}
 * can be modified with Java's {@link java.util.ServiceLoader ServiceLoader}, the abstraction is intentionally
 * coupled with Guice's {@link com.google.inject.Injector Injector}.
 *
 * @see com.arcbees.shell.injector.InjectorFactory InjectorFactory
 */
package com.arcbees.shell.injector;
