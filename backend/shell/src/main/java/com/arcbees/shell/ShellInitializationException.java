package com.arcbees.shell;

/**
 * An exception that can be thrown by the different actors involved in bootstrapping the {@link ArcbeesShell}. An
 * {@link ShellExecutionResult} can optionally be provided to tell the shell that this exception is not an actual
 * error. By default the execution result is set to {@link ShellExecutionResult#ERROR}.
 */
public class ShellInitializationException extends Exception {
    private final ShellExecutionResult executionResult;

    public ShellInitializationException(String message) {
        this(message, ShellExecutionResult.ERROR);
    }

    public ShellInitializationException(String message, ShellExecutionResult executionResult) {
        super(message);

        this.executionResult = executionResult;
    }

    public ShellInitializationException(String message, Throwable cause) {
        this(message, cause, ShellExecutionResult.ERROR);
    }

    public ShellInitializationException(String message, Throwable cause, ShellExecutionResult executionResult) {
        super(message, cause);

        this.executionResult = executionResult;
    }

    public ShellExecutionResult getExecutionResult() {
        return executionResult;
    }
}
