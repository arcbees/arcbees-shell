package com.arcbees.shell;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.arcbees.shell.application.Application;
import com.arcbees.shell.application.ApplicationService;
import com.arcbees.shell.environment.Environment;
import com.arcbees.shell.environment.EnvironmentFactory;
import com.arcbees.shell.environment.GlobalEnvironment;
import com.arcbees.shell.injector.InjectorFactory;
import com.google.common.collect.ImmutableSet;
import com.google.inject.ConfigurationException;
import com.google.inject.Injector;
import com.google.inject.Module;

import static java.util.Collections.singletonList;

/**
 * The `ArcbeesShell` is used to bootstrap an application. All the configuration is done fluently through
 * {@link ArcbeesShellConfiguration} by calling {@link #configure()}. This is intended to be done in your Main class.
 *
 * The shell will coordinate the bootstrap sequence. It will, in order execute the following steps:
 *
 * 1. Find an appropriate environment factory with {@link EnvironmentFactory#load()};
 * 2. Create {@link Environment} instances by using the environment factory;
 * 3. Find an appropriate injector factory with {@link InjectorFactory#load()};
 * 4. Create an {@link Injector} instance by uaing the injector factory;
 * 5. Configure global JVM parameters, defined in {@link GlobalEnvironment};
 * 6. Create an instance of {@link Application};
 * 7. and finally run it.
 *
 * An example bootstrap configuration might look like:
 * ```java
 * public class Main {
 *     public static void main(String[] args) {
 *         ArcbeesShell.configure()
 *                 .name("Arcbees Is Awesome")
 *                 .arguments(args)
 *
 *                 .addModule(InterfacesModule.class)
 *                 .addModule(InfrastructureModule.class)
 *                 .addModule(ApplicationModule.class)
 *                 .addApplicationService(HttpServer.class)
 *
 *                 .addEnvironment(ApplicationEnvironment.class)
 *                 .addEnvironment(InterfaceEnvironment.class)
 *                 .addEnvironment(SqlEnvironment.class)
 *
 *                 .start();
 *     }
 * }
 * ```
 */
public class ArcbeesShell {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArcbeesShell.class);
    private static final String COULD_NOT_INIT_SHELL = "The Arcbees Shell could not be properly initialized.";

    private final String name;
    private final Collection<Class<? extends Environment>> environmentClasses;
    private final Set<Class<? extends Module>> moduleClasses;
    private final Set<Class<? extends ApplicationService>> applicationServiceClasses;
    private final String[] arguments;

    static {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }

    ArcbeesShell(ArcbeesShellConfiguration configuration) {
        this.name = configuration.name;
        this.environmentClasses = ImmutableSet.copyOf(configuration.environmentClasses);
        this.moduleClasses = ImmutableSet.copyOf(configuration.moduleClasses);
        this.applicationServiceClasses = ImmutableSet.copyOf(configuration.applicationServiceClasses);
        this.arguments = Arrays.copyOf(configuration.arguments, configuration.arguments.length);
    }

    /**
     * Configure your {@link ArcbeesShell} application.
     *
     * @return an instance of {@link ArcbeesShellConfiguration} to customize your {@link ArcbeesShell} application.
     */
    public static ArcbeesShellConfiguration configure() {
        return new ArcbeesShellConfiguration();
    }

    /**
     * Execute the bootstrap sequence, as described by {@link ArcbeesShell} and exit the JVM when the program completes.
     *
     * This is the same as calling {@link #start(boolean)} with `true`.
     */
    public void start() {
        start(true);
    }

    /**
     * Execute the bootstrap sequence, as described by {@link ArcbeesShell}.
     *
     * If `exit` is `true`, the JVM will exit when the program completes, otherwise the {@link ShellExecutionResult}
     * will be returned.
     *
     * {@link System#exit(int)} is called to exit the JVM, so you won't be able to perform
     * additional tear down logic. Pass `false` to this method and handle the JVM exit in your application if this is
     * an issue. This can be particularly helpful in test scenarios where you don't want the JVM to exit prematurely.
     *
     * @param exit `true` to exit the JVM when the program completes, `false` otherwise
     * @return the execution result, if `exit` is `false`
     */
    public ShellExecutionResult start(boolean exit) {
        ShellExecutionResult executionResult;

        try {
            executionResult = bootstrap();
        } catch (ShellInitializationException e) {
            executionResult = e.getExecutionResult();
            if (executionResult != ShellExecutionResult.SUCCESS) {
                LOGGER.error(COULD_NOT_INIT_SHELL, e);
            }
        } catch (Throwable e) {
            executionResult = ShellExecutionResult.ERROR;
            LOGGER.error(COULD_NOT_INIT_SHELL, e);
        }

        if (exit) {
            System.exit(executionResult.getCode());
        }
        return executionResult;
    }

    private ShellExecutionResult bootstrap() throws ShellInitializationException {
        Module environmentModule = initializeEnvironment();
        Injector injector = createInjector(environmentModule);

        configureGlobalProperties(injector);

        return runApplication(injector);
    }

    private Module initializeEnvironment() throws ShellInitializationException {
        return EnvironmentFactory.load().create(name, environmentClasses, arguments);
    }

    private Injector createInjector(Module environmentModule) throws ShellInitializationException {
        List<Module> modules = singletonList(new ShellModule(applicationServiceClasses));
        return InjectorFactory.load().create(environmentModule, environmentClasses, moduleClasses, modules);
    }

    private void configureGlobalProperties(Injector injector) throws ShellInitializationException {
        GlobalEnvironment environment = getGlobalEnvironment(injector);

        TimeZone.setDefault(TimeZone.getTimeZone(environment.getTimezone()));
        LOGGER.info("Default time zone set to {}.", TimeZone.getDefault().getDisplayName());

        Locale.setDefault(Locale.forLanguageTag(environment.getLocale()));
        LOGGER.info("Default locale set to {}.", Locale.getDefault().getDisplayName());
    }

    private GlobalEnvironment getGlobalEnvironment(Injector injector) throws ShellInitializationException {
        try {
            return injector.getInstance(GlobalEnvironment.class);
        } catch (ConfigurationException e) {
            throw new ShellInitializationException("Could not find an implementation of GlobalEnvironment. "
                    + "Make sure it is bound in a Guice module.", e);
        }
    }

    private ShellExecutionResult runApplication(Injector injector) {
        Application application = injector.getInstance(Application.class);
        return application.run();
    }
}
