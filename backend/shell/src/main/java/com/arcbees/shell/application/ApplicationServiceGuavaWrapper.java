package com.arcbees.shell.application;

import com.google.common.util.concurrent.AbstractService;

class ApplicationServiceGuavaWrapper extends AbstractService {
    private final ApplicationService service;
    private final String name;

    ApplicationServiceGuavaWrapper(ApplicationService service) {
        this.service = service;
        this.name = service.getClass().getSimpleName();
    }

    @Override
    protected void doStart() {
        try {
            service.start();
            notifyStarted();
        } catch (Exception e) {
            notifyFailed(e);
        }
    }

    @Override
    protected void doStop() {
        try {
            service.stop();
            notifyStopped();
        } catch (Exception e) {
            notifyFailed(e);
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
