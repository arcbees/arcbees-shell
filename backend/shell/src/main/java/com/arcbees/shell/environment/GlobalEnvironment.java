package com.arcbees.shell.environment;

import java.util.Collection;

/**
 * A global {@link Environment} containing parameters required to properly bootstrap the
 * {@link com.arcbees.shell.ArcbeesShell ArcbeesShell}. An implementation of this class must be configured in the
 * {@link com.google.inject.Module Module} returned by {@link EnvironmentFactory#create(String, Collection, String[])
 * EnvironmentFactory#create(...)}.
 */
public interface GlobalEnvironment extends Environment {
    String getLocale();

    String getTimezone();
}
