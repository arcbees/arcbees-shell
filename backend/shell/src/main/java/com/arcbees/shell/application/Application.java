package com.arcbees.shell.application;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arcbees.shell.ShellExecutionResult;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.Service.State;
import com.google.common.util.concurrent.ServiceManager;
import com.google.common.util.concurrent.ServiceManager.Listener;

import static java.lang.Runtime.getRuntime;
import static java.util.stream.Collectors.toSet;

/**
 * The `Application` is responsible for starting and stopping {@link ApplicationService}s. When all services are
 * stopped, an {@link ShellExecutionResult} corresponding to the worst service result will be returned.
 *
 * @see #run()
 */
public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private static final int SHUTDOWN_TIMEOUT_SECONDS = 10;

    private final ServiceManager serviceManager;

    @Inject
    Application(Set<ApplicationService> services) {
        Set<ApplicationServiceGuavaWrapper> serviceWrappers = services.stream()
                .map(ApplicationServiceGuavaWrapper::new)
                .collect(toSet());

        serviceManager = new ServiceManager(serviceWrappers);
        serviceManager.addListener(new LoggingListener());
    }

    /**
     * Start all new {@link ApplicationService}s. This should never be called directly by applications unless you
     * know exactly what you are doing. The `Arcbees Shell` is normally responsible for starting the application.
     *
     * This method will wait until all {@link ApplicationService}s are done running before returning.
     *
     * @return {@link ShellExecutionResult#ERROR} if any service failed, {@link ShellExecutionResult#SUCCESS} otherwise
     * @throws IllegalStateException if no new services {@link ApplicationService} were added since the last call to
     * `run()`
     */
    public ShellExecutionResult run() {
        Thread shutdownThread = new Thread(this::shutDown, "ApplicationShutdown");
        getRuntime().addShutdownHook(shutdownThread);

        serviceManager.startAsync().awaitStopped();

        return getExitStatus();
    }

    private void shutDown() {
        LOGGER.debug("Shut down requested. Making sure all services are gracefully stopped.");

        try {
            serviceManager
                    .stopAsync()
                    .awaitStopped(SHUTDOWN_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            LOGGER.warn("Could not shutdown services in less than {} seconds.", SHUTDOWN_TIMEOUT_SECONDS);
        }
    }

    private ShellExecutionResult getExitStatus() {
        return serviceManager.servicesByState().get(State.FAILED).isEmpty()
                ? ShellExecutionResult.SUCCESS
                : ShellExecutionResult.ERROR;
    }

    static class LoggingListener extends Listener {
        LoggingListener() {
        }

        @Override
        public void healthy() {
            LOGGER.info("All services are started and healthy.");
        }

        @Override
        public void stopped() {
            LOGGER.info("All services are stopped.");
        }

        @Override
        public void failure(Service service) {
            LOGGER.error("Service '{}' failed. Trying to run other services, you probably want to investigate this.",
                    service, service.failureCause());
        }
    }
}
