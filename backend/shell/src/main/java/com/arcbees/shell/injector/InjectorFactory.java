package com.arcbees.shell.injector;

import java.util.Collection;
import java.util.Iterator;
import java.util.ServiceLoader;

import com.arcbees.shell.ShellInitializationException;
import com.arcbees.shell.environment.Environment;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * A factory used to create the application {@link Injector}. It is possible to provide a custom implementation by
 * using Java's {@link ServiceLoader}.
 *
 * @see #create(Module, Collection, Collection, Collection)
 */
public interface InjectorFactory {
    /**
     * Load the first {@link InjectorFactory} implementation available in the classpath. Java's {@link ServiceLoader}
     * is used to load available implementations.
     *
     * @return the {@link InjectorFactory} that will be used to inject the application
     * @throws ShellInitializationException if no implementations exist in the classpath
     */
    static InjectorFactory load() throws ShellInitializationException {
        Iterator<InjectorFactory> iterator = ServiceLoader.load(InjectorFactory.class).iterator();
        if (!iterator.hasNext()) {
            throw new ShellInitializationException(
                    "No implementations of InjectorFactory exist. Make sure your classpath exports one.");
        }

        return iterator.next();
    }

    /**
     * Create an injector of the provided application modules. The creation is performed in three steps:
     *
     * 1. First construct an {@link Injector} with an environment implementation module and all {@link Environment}
     * classes bound in {@link javax.inject.Singleton}.
     * 2. Create an instance of the application modules with the previous injector. This allows individual modules to
     * inject environment classes and control bindings based on environment values.
     * 3. Create a final {@link Injector} with the instantiated application modules and environment module.
     *
     * @param environmentImplementationModule a module that contains bindings for the environment implementation
     * @param environmentClasses the {@link Environment} classes configured by the application
     * @param injectableModuleClasses the {@link Module} classes configured by the application
     * @param otherModules the {@link Module} other modules required for the bootstrap sequence
     *
     * @return the {@link Injector} that will be used to create an
     * {@link com.arcbees.shell.application.Application Application} and initiate the boot sequence
     *
     * @throws ShellInitializationException if the bootstrap sequence fails at any point
     */
    Injector create(
            Module environmentImplementationModule,
            Collection<Class<? extends Environment>> environmentClasses,
            Collection<Class<? extends Module>> injectableModuleClasses,
            Collection<Module> otherModules) throws ShellInitializationException;
}
