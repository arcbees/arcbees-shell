package com.arcbees.shell;

import java.util.Collection;

import javax.inject.Singleton;

import com.arcbees.shell.application.Application;
import com.arcbees.shell.application.ApplicationService;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import static com.google.inject.multibindings.Multibinder.newSetBinder;

/**
 * A Guice module used to configure the global shell dependencies.
 */
class ShellModule extends AbstractModule {
    private final Collection<Class<? extends ApplicationService>> applicationServices;

    ShellModule(Collection<Class<? extends ApplicationService>> applicationServices) {
        this.applicationServices = applicationServices;
    }

    @Override
    protected void configure() {
        bind(Application.class).in(Singleton.class);
        bindApplicationServices();
    }

    private void bindApplicationServices() {
        Multibinder<ApplicationService> multibinder = newSetBinder(binder(), ApplicationService.class);
        applicationServices.forEach(c -> multibinder.addBinding().to(c).in(Singleton.class));
    }
}
