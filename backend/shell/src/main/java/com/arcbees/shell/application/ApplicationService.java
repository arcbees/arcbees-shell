package com.arcbees.shell.application;

/**
 * An application service that has its lifecycle managed by the ArcbeesShell. This can be anything that can be broken
 * up in a different application such as a REST API, a cron scheduler or a message relay. This is useful if you don't
 * actually need your application to be broken up in multiple pieces but still want to preserve the logical split.
 *
 * @see Application
 */
public interface ApplicationService {
    /**
     * Start the application service.
     *
     * @throws Exception if anything happens during the running phase
     */
    default void start() throws Exception {
    }

    /**
     * Stop the application service. This is usually called when the {@link Application} is required to stop all
     * running services.
     *
     * @throws Exception if anything happens during the stopping phase
     */
    default void stop() throws Exception {
    }
}
