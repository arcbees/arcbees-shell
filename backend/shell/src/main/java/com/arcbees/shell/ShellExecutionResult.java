package com.arcbees.shell;

/**
 * An execution result code that will be returned when all
 * {@link com.arcbees.shell.application.ApplicationService ApplicationService} are stopped.
 */
public enum ShellExecutionResult {
    /** Indicate that no errors occurred. Status code: 0 */
    SUCCESS(0),
    /** Indicate that something wrong occurred during the execution. Status code: 1 */
    ERROR(1);

    private final int code;

    ShellExecutionResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
