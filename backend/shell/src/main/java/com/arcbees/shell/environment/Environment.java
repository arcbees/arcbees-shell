package com.arcbees.shell.environment;

/**
 * A marker interface to describe classes containing various environment parameters. These classes must be configured
 * through
 * {@link com.arcbees.shell.ArcbeesShellConfiguration#addEnvironment(Class) ArcbeesShellConfiguration#addEnvironment()}.
 * Instances must be injectable with an {@link javax.inject.Inject @Inject}able constructor or a default constructor.
 *
 * Environments can be injected a limited set of classes. Typically, input methods provided by the environment
 * implementation available in the classpath.
 *
 * @see EnvironmentFactory
 */
public interface Environment {
}
