package com.arcbees.shell.injector;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.arcbees.shell.ShellInitializationException;
import com.arcbees.shell.environment.Environment;
import com.google.inject.ConfigurationException;
import com.google.inject.CreationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.ProvisionException;

import static java.util.stream.Collectors.toSet;

public class GuiceInjectorFactory implements InjectorFactory {
    @Override
    public Injector create(
            Module environmentImplementationModule,
            Collection<Class<? extends Environment>> environmentClasses,
            Collection<Class<? extends Module>> moduleClasses,
            Collection<Module> otherModules) throws ShellInitializationException {
        try {
            Module environmentModule = new EnvironmentModule(
                    environmentImplementationModule,
                    environmentClasses,
                    moduleClasses);
            Injector environmentInjector = Guice.createInjector(environmentModule);
            Collection<Module> injectableModules = moduleClasses.stream()
                    .map(environmentInjector::getInstance)
                    .collect(toSet());

            Set<Module> modules = new HashSet<>();
            modules.add(environmentModule);
            modules.addAll(injectableModules);
            modules.addAll(otherModules);

            return Guice.createInjector(modules);
        } catch (CreationException | ConfigurationException | ProvisionException e) {
            throw new ShellInitializationException("Unable to create Guice Injector.", e);
        }
    }
}
