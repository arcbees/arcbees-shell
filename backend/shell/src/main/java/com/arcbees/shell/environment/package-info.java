/**
 * An environment is basically a data class that can be used to provide different configuration values dynamically.
 *
 * Environments are also the only classes that can be injected into Guice
 * {@linkplain com.google.inject.Module modules}, so you can create bindings conditionally. How these values are
 * populated is totally up to the application. By default, a
 * {@link com.arcbees.shell.environment.GlobalEnvironment GlobalEnvironment} is present and **requires** an
 * implementation or the application will crash. Some libraries like the *CLI* will
 * provide both this implementation and a mean to input environment values.
 *
 * @see com.arcbees.shell.environment.Environment
 */
package com.arcbees.shell.environment;
