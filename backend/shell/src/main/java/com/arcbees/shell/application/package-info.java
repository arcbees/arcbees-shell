/**
 * An application service is an application by itself, but its lifecycle is managed by the Arcbees Shell.
 *
 * It can be an HTTP server, a cron scheduler or really anything. The rationale is that whilst many applications may
 * need to create multiple applications to handle different concerns, some smaller ones may benefit from a single
 * program aggregating all of their jobs.
 *
 * @see com.arcbees.shell.application.ApplicationService
 */
package com.arcbees.shell.application;
