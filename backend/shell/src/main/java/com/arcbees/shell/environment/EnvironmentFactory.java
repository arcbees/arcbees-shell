package com.arcbees.shell.environment;

import java.util.Collection;
import java.util.Iterator;
import java.util.ServiceLoader;

import com.arcbees.shell.ShellInitializationException;
import com.google.inject.Module;

/**
 * A factory used to create a {@link Module} used by the
 * {@link com.arcbees.shell.injector.InjectorFactory InjectorFactory} to configure {@link Environment}s. It is
 * possible to provide a custom implementation by using Java's {@link ServiceLoader}.
 *
 * @see #create(String, Collection, String[])
 */
public interface EnvironmentFactory {
    /**
     * Load the first {@link EnvironmentFactory} implementation available in the classpath. Java's {@link ServiceLoader}
     * is used to load available implementations.
     *
     * @return the {@link EnvironmentFactory} that will be used to configure the application's {@link Environment}
     * @throws ShellInitializationException if no implementations exist in the classpath
     */
    static EnvironmentFactory load() throws ShellInitializationException {
        Iterator<EnvironmentFactory> iterator = ServiceLoader.load(EnvironmentFactory.class).iterator();
        if (!iterator.hasNext()) {
            throw new ShellInitializationException(
                    "No implementations of EnvironmentFactory exist. Make sure your classpath exports one.");
        }

        return iterator.next();
    }

    /**
     * Create a {@link Module} used to configure the application {@link Environment}s.
     *
     * @param name the static name of the application, configured with
     * {@link com.arcbees.shell.ArcbeesShellConfiguration#name(String) ArcbeesShellConfiguration#name(String)}
     * @param environmentClasses the {@link Environment} classes that will be managed, configured with
     * {@link com.arcbees.shell.ArcbeesShellConfiguration#addEnvironment(Class)
     * ArcbeesShellConfiguration#addEnvironment(Class)}
     * @param args the arguments used to configure {@link Environment}s, configured with
     * {@link com.arcbees.shell.ArcbeesShellConfiguration#arguments(String[])
     * ArcbeesShellConfiguration#arguments(String[])}
     * @return a {@link Module} used to create {@link Environment}s
     * @throws ShellInitializationException if the configuration fails at any points
     */
    Module create(String name, Collection<Class<? extends Environment>> environmentClasses, String[] args)
            throws ShellInitializationException;
}
