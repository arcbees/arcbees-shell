/**
 * The Arcbees Shell is the entry point of everything that exists; it manages the bootstrap process of the application.
 *
 * It does nothing alone, yet it starts application services like an http server or a scheduler, defined by the client
 * application. The Shell expects applications to use [Guice](https://github.com/google/guice/wiki/GettingStarted) and
 * will take the creation of the Injector in charge.
 *
 * Two main concepts:
 *
 * * {@link com.arcbees.shell.application Application service}
 * * {@link com.arcbees.shell.environment.Environment Environment}
 *
 * @see com.arcbees.shell.ArcbeesShell ArcbeesShell
 */
package com.arcbees.shell;
