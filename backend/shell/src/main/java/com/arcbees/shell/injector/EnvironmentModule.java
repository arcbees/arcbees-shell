package com.arcbees.shell.injector;

import java.util.Collection;

import javax.inject.Singleton;

import com.arcbees.shell.environment.Environment;
import com.google.inject.AbstractModule;
import com.google.inject.Module;

/**
 * A Guice module used to configiure bindings for {@link Environment} and {@link Module} classes.
 */
class EnvironmentModule extends AbstractModule {
    private final Module implementationModule;
    private final Collection<Class<? extends Environment>> environmentClasses;
    private final Collection<Class<? extends Module>> moduleClasses;

    EnvironmentModule(
            Module implementationModule,
            Collection<Class<? extends Environment>> environmentClasses,
            Collection<Class<? extends Module>> moduleClasses) {
        this.implementationModule = implementationModule;
        this.environmentClasses = environmentClasses;
        this.moduleClasses = moduleClasses;
    }

    @Override
    protected void configure() {
        install(implementationModule);

        environmentClasses.forEach(c -> bind(c).in(Singleton.class));
        moduleClasses.forEach(c -> bind(c).in(Singleton.class));
    }
}
