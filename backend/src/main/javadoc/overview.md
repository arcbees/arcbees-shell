The Arcbees Shell is an opinionated set of libraries to facilitate software development at [Arcbees](https://www.arcbees.com/).

The goal of this project is to facilitate application development at Arcbees. The approaches taken are opinionated and may or may not apply to every project. That's why we tried to create multiple libraries to be used on a case-by-case basis. We have a central point, the Shell library, which introduces some abstract core concept and inflexion points. From there, other libraries can be added to implement those ideas.

---

**Libraries Documentation:**

* {@linkplain com.arcbees.shell The core library}
* {@linkplain com.arcbees.shell.cli The CLI library}
