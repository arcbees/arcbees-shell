# Arcbees Shell
The Arcbees Shell is an opinionated set of libraries to facilitate software development at [Arcbees][arcbees].

## License
The libraries are publicly available under the Apache 2.0 license. Source and Javadoc jars are deployed and available through Maven Central.

## Support
We do not guaranty support of any kind for our libraries under the `com.arcbees.shell` group id. We are however open to discussions and as such you can contact us at [queenbee@arcbees.com][mailto-queenbee].

## Design Goals
The Arcbees Shell goal is to facilitate application development at Arcbees. The approaches taken are opinionated and may or may not apply to every project. That's why we tried to create multiple libraries to be used on a case-by-case basis. We have a central point, the Shell library, which introduces some abstract core concept and inflexion points. From there, other libraries can be added to implement those ideas.

## Usage
**Don't expect a nice Getting Started guide** (*unless someone proposes himself to write and maintain one*). Most classes and public methods are documented with **[Javadoc][javadoc]** and some even include usage examples. So go on and [read the Javadoc][javadoc]. If that's not enough, our recommended application structure can also be grabbed by running the [Arcbees's Yeoman generator][yo-arcbees-repo].


[arcbees]: https://www.arcbees.com/
[javadoc]: shell/src/main/java/com/arcbees/shell/package-info.java
[mailto-queenbee]: mailto:queenbee@arcbees.com
[yo-arcbees-repo]: https://bitbucket.org/arcbees/yo-arcbees
