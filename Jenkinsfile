#!/usr/bin/env groovy

@Library('arcbees-workflow') _

pipeline {
    agent any
    options {
        ansiColor('xterm')
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }
    tools {
        maven 'Maven'
    }
    environment {
        BRANCH_STAGING = 'master'
        BRANCH_RELEASE = 'release'
        BB_CREDS_ID = 'OAuth_bitbucket_consumer'
        M2_HOME = '/data/maven'
    }

    stages {

        stage('Backend: Compile') {
            steps {
                dir('backend') {
                    sh 'mvn -B -U package -P package-only'
                }
            }
            post {
                success {
                    archiveArtifacts artifacts: 'backend/**/target/*.jar', fingerprint: true
                }
            }
        } // End Prepare Backend

        stage('Test') {
            parallel {

                stage('Backend: Checkstyle') {
                    environment {
                        BB_KEY = 'backend_checkstyle'
                    }
                    steps {
                        bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        dir('backend') {
                            failAsUnstable { sh 'mvn -B validate' }
                        }
                    }
                    post {
                        success {
                            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        failure {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        unstable {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                    }
                } // End Checkstyle

                stage('Backend: Unit Tests') {
                    environment {
                        BB_KEY = 'backend_ut'
                    }
                    steps {
                        bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        dir('backend') {
                            failAsUnstable { sh 'mvn -B test -Dcheckstyle.skip -DskipITs' }
                        }
                    }
                    post {
                        always {
                            dir('backend') {
                                junit(testResults: '**/target/surefire-reports/*.xml', healthScaleFactor: 1)
                                jacoco(execPattern: '**/**.exec', sourcePattern: '**/src/main/java', exclusionPattern: '**/*Test*.class', inclusionPattern: '**/*.class', classPattern: '**/classes')
                            }
                        }
                        success {
                            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        failure {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        unstable {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                    }
                } // End Backend: Test

                stage('Backend: Integration Tests') {
                    environment {
                        BB_KEY = 'backend_it'
                    }
                    steps {
                        bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        dir('backend') {
                            failAsUnstable { sh 'mvn -B verify -Dcheckstyle.skip -DskipUTs' }
                        }
                    }
                    post {
                        always {
                            dir('backend') {
                                junit(testResults: '**/target/failsafe-reports/*.xml', healthScaleFactor: 1)
                            }
                        }
                        success {
                            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        failure {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                        unstable {
                            bitbucketStatusNotify(buildState: 'FAILED', buildKey: env.BB_KEY, buildName: env.STAGE_NAME, credentialsId: env.BB_CREDS_ID)
                        }
                    }
                } // End Backend: Test

            }
        } // End: Test

        stage('Generate Sources') {
            parallel {
                stage('Generate Source JARs') {
                    steps {
                        dir('backend') {
                            sh 'mvn -B verify -P package-only -P generate-source-jars'
                        }
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'backend/**/target/*-sources.jar', fingerprint: true
                            archiveArtifacts artifacts: 'backend/**/target/*-javadoc.jar', fingerprint: true
                        }
                    }
                } // End: Generate Source JARs

                stage('Generate Javadoc Site') {
                    steps {
                        dir('backend') {
                            sh 'mvn -B javadoc:aggregate@all-modules'
                        }
                    }
                    post {
                        success {
                            publishHTML([
                                    reportDir            : 'backend/target/site/apidocs/',
                                    reportFiles          : 'index.html',
                                    reportName           : 'Javadoc',
                                    reportTitles         : 'Javadoc',
                                    allowMissing         : false,
                                    alwaysLinkToLastBuild: false,
                                    keepAll              : true
                            ])
                        }
                    }
                } // End: Generate Javadoc Site
            }
        } // End: Generate Sources

        stage('Publish') {
            when {
                expression { currentBuild.resultIsBetterOrEqualTo('SUCCESS') }
                anyOf {
                    branch env.BRANCH_STAGING
                    branch env.BRANCH_RELEASE
                }
            }

            parallel {
                stage('Publish Snapshot') {
                    when {
                        branch env.BRANCH_STAGING
                    }
                    steps {
                        dir('backend') {
                            sh 'mvn -B deploy -P package-only -P sign'
                        }
                    }
                }

                stage('Publish Stable') {
                    when {
                        branch env.BRANCH_RELEASE
                    }
                    steps {
                        sh 'mvn -B verify -P package-only -P sign'
                        fail('Stable release publication is not configured')
                    }
                }
            }
        } // End: Publish
    }

    post {
        always {
            cleanWs(notFailBuild: true)
        }
    }
}
